const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})



	app.post('/rates', (req, res) => {
    	const alias = req.body.alias;
    	const name = req.body.name;
    	const ex = req.body.ex;

	    if (!alias || !name || !ex) {
	        return res.status(400).send({
	            'error': 'Bad Request - Missing required parameters'
	        });
	    }

	    if (typeof alias !== 'string' || typeof name !== 'string' || typeof ex !== 'object') {
	        return res.status(400).send({
	            'error': 'Bad Request - Invalid input types'
	        });
	    }

	    if (alias === '' || name === '' || Object.keys(ex).length === 0) {
	        return res.status(400).send({
	            'error': 'Bad Request - input field is empty'
	        });
	    }

	    if (exchangeRates[alias]) {
	        return res.status(400).send({
	            'error': 'Bad Request - Currency is already documented'
	        });
	    }
	    
	    return res.status(200).send({
        'Posted': 'All fields are complete'
    	});
	});

}
