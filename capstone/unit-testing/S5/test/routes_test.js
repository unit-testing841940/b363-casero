const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {

	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})


	it('test_api_post_rates_returns_is_running', () => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})		
	})


	it('test_api_post_rates_returns_400_if_param_name_is_missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
	        alias: "eur",
	        ex:{
	        	peso: 60.00,
      			won: 1300.00,
      			yen: 120.00,
      			yuan: 8.00      	
	        }
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	        done()
	    })
	})
	
	it('test_api_post_rates_returns_400_if_param_name_is_not_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
	        alias: "eur",
	        name: 3,
	        ex:{
	        	peso: 60.00,
      			won: 1300.00,
      			yen: 120.00,
      			yuan: 8.00      	
	        }
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	    })
	    done()
	})

	
	it('test_api_post_rates_returns_400_if_param_name_is_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
	        alias: "eur",
	        name: '',
	        ex:{
	        	peso: 60.00,
      			won: 1300.00,
      			yen: 120.00,
      			yuan: 8.00      	
	        }
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	    })
	    done()
	})
	
	it('test_api_post_rates_returns_400_if_param_ex_missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
	        alias: "eur",
	        name: "Yuro"
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	    })
	    done()
	})
	

	it('test_api_post_rates_returns_400_if_param_ex_is_not_an_object', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
	        alias: "eur",
	        name: "Yuro",
	        ex:'stringy'
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	    })
	    done()
	})
	

	it('test_api_post_rates_returns_400_if_param_ex_is_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
	        alias: "eur",
	        name: "Yuro",
	        ex: {}
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	    })
	    done()
	})
	
	it('test_api_post_rates_returns_400_if_param_alias_is_missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
	        name: "Yuro",
	        ex: {
	        	peso: 60.00,
      			won: 1300.00,
      			yen: 120.00,
      			yuan: 8.00      	
	        }
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	    })
	    done()
	})
	
	it('test_api_post_rates_returns_400_if_param_alias_is_not_a_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			alias: [],
	        name: "Yuro",
	        ex: {
	        	peso: 60.00,
      			won: 1300.00,
      			yen: 120.00,
      			yuan: 8.00      	
	        }
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	    })
	    done()
	})
	

	it('test_api_post_rates_returns_400_if_param_alias_is_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			alias: '',
	        name: "Yuro",
	        ex: {
	        	peso: 60.00,
      			won: 1300.00,
      			yen: 120.00,
      			yuan: 8.00      	
	        }
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	    })
	    done()
	})

	it('test_api_post_rates_returns_400_if_param_is_duplicate', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			alias: 'peso',
	        name: 'Philippine Peso',
	        ex: {
	        	peso: 60.00,
      			won: 1300.00,
      			yen: 120.00,
      			yuan: 8.00      	
	        }
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	    })
	    done()
	})

	it('test_api_post_rates_returns_200_if_all_params_are_complete', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			alias: 'eur',
	        name: "Yuro",
	        ex: {
	        	peso: 60.00,
      			won: 1300.00,
      			yen: 120.00,
      			yuan: 8.00      	
	        }
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(200);
	    })
	    done()
	})

})
