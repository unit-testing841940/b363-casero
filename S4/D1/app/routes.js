const {names,  users} = require('../src/util.js');

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({'data': {} });
    });

    app.get('/people', (req, res) => {
        return res.send({
            people: names
        });
    })

    app.post('/person', (req, res) => {
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }
        if(!req.body.hasOwnProperty('age')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter AGE'
            })
        }
        if(typeof req.body.age !== "number"){
            return res.status(400).send({
                'error': 'Bad Request - AGE has to be a number'
            })  
        }
        if(!req.body.hasOwnProperty('username')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter username'
            })  
        }
    })


    //Activity 4
     app.post('/login',(req,res)=>{

        let foundUser = users.find((user) => {

              return user.username === req.body.username && user.password === req.body.password;
        });

        if(!req.body.hasOwnProperty('username')){
            // Solution here
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter username'
            })
        }

        if(!req.body.hasOwnProperty('password')) {
            // Solution here
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter password'
            })
        }

        if(foundUser){
            // Solution here
            return res.status(200).send({
                'good':'credentials are good'
            })
        }

        if(!foundUser){
            // Solution here
            return res.status(403).send({
                'error':'credentials are wrong!'
            })
        }
    })
}