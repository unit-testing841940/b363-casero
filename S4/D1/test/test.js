const { factorial, div_check } = require('../src/util.js')

const {expect, assert} = require('chai')

describe('test_fun_factorial', () =>{

	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120)
	})

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1)
	})

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0);
		assert.equal(product, 1)
	})

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4);
		assert.equal(product, 24)
	})

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10);
		assert.equal(product, 3628800)
	})

	it('test_fun_factorial_neg_1_is_undefined', () => {
		const product = factorial(-1);
		expect(product).to.equal(undefined)
	})

	it('test_fun_factorial_entering_inavlid_input_returns_undefined', () => {
		const product = factorial('a');
		expect(product).to.equal(undefined)
	})
	/*
	it('test_fun_factorial_entering_inavlid_input_returns_undefined', () => {
		const product = 'a';
		expect(product).to.equal(undefined)
	})
	*/
	
})


describe('test_div_check', () =>{


	it('test_div_check_21!_is_true', () => {
		const divi = div_check(21);
		assert.equal(divi, true)
	})

	it('test_div_check_13!_is_false', () => {
		const divi = div_check(13);
		assert.equal(divi, false)
	})

	it('test_div_check_30!_is_true', () => {
		const divi = div_check(30);
		expect(divi).to.equal(true)
	})

	it('test_div_check_11!_is_false', () => {
		const divi = div_check(11);
		expect(divi).to.equal(false)
	})

})

